exports.requestGroupRate = function (req, res) {
    res.render('request=group-rate');
}

exports.requestGroupRateProcessPost = function (req, res, next) {
    next(new Error('Запрос группы еще не реализован'));
}

exports.home = function (req, res, next) {
    next(new Error('Контактная страница еще не реализована'));
}

exports.homeProcessPost = function (req, res, next) {
    next(new Error('Контактная страница еще не реализован!'));
}