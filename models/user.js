var moongoose = require('mongoose');

var userSchema = moongoose.Schema({
    authId: String,
    name: String,
    email: String,
    role: String,
    created: Date
});

var User = moongoose.model('User', userSchema);
module.exports = User;