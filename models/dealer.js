var mongoose = require('mongoose');

var dealerSchema = mongoose.Schema({
    name: String,
    address1: String,
    address2: String,
    city: String,
    zip: String,
    country: String,
    phone: String,
    website: String,
    active: String,
    geocodedAddress: String,
    lat: Number,
    lng: Number
});

dealerSchema.methods.getAddress = function (lineDelim) {
    if (!lineDelim) lineDelim = '<br>';
    var addr = this.address1;
    if (this.address2 && this.address2.math(/\S/)) {
        addr += lineDelim + this.address2;
    }
    addr += lineDelim + this.city + ', ' + this.zip;
    addr += lineDelim + (this.country || 'US');
    return addr;
};

var Dealer = mongoose.model("Dealer", dealerSchema);
module.exports = Dealer;