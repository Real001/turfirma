module.exports = {
    bundles: {
        clientJavaScript: {
            main: {
                file: '/js.min/seaofdreamtravel.min.e58c6082.js',
                location: 'head',
                contents: [
                    '/js/contact.js',
                    '/js/cart.js'
                ]
            }
        },
        clientCss: {
            main: {
                file: '/css/seaofdreamtravel.min.771b6427.css',
                contents: [
                    '/css/main.css',
                    '/css/cart.css'
                ]
            }
        }
    }
}