module.exports = function (grunt) {
    //загрузка плагинов
    [
        'grunt-cafe-mocha',
        'grunt-contrib-jshint',
        'grunt-exec',
        'grunt-contrib-less',
        'grunt-contrib-uglify',
        'grunt-contrib-cssmin',
        'grunt-hashres'
    ].forEach(function (task) {
        grunt.loadNpmTasks(task);
    });

    //настройка плагинов
    grunt.initConfig({
        cafemocha: {
            all: {src: 'qa/test-*.js', options: {ui: 'tdd'}}
        },
        jshint: {
            app: ['seaofdreamtravel.js', 'public/js/**/*.js', 'lib/**/*.js'],
            qa: ['Gruntfile.js', 'public/qa/**/*.js', 'qa/**/*.js']
        },
        exec: {
            linkchecker: {
                cmd: 'linkchecker http://localhost:3000'
            }
        },
        less: {
            development: {
                option: {
                  customFunctions: {
                      static: function (lessObject, name) {
                          return 'url("'+ require('./lib/static.js').map(name.value) + '")';
                      }
                  }
                },
                files: {
                    'public/css/main.css': 'less/main.less',
                    'public/css/cart.css': 'less/cart.less'
                }
            }
        },
        uglify: {
            all: {
                files: {
                    'public/js.min/seaofdreamtravel.min.js': ['public/js/**/*.js']
                }
            }
        },
        cssmin: {
            combine: {
                files: {
                    'public/css/seaofdreamtravel.css': ['public/css/**/*.css',
                    '!public/css/seaofdreamtravel*.css']
                }
            },
            minify: {
                src: 'public/css/seaofdreamtravel.css',
                dest: 'public/css/seaofdreamtravel.min.css'
            }
        },
        hashres: {
            options: {
                fileNameFormat: '${name}.${hash}.${ext}'
            },
            all: {
                src: [
                    'public/js.min/seaofdreamtravel.min.js',
                    'public/css/seaofdreamtravel.min.css'
                ],
                dest: [
                    'config.js'
                ]

            }
        },
        lint_pattern: {
            view_static: {
                options: {
                    rules: [
                        {
                            pattern: /<link [^>]*href=["'](?!\{\{|(https?:)?\/\/)/,
                            message: 'В <link> обнаружен статический ресурс, которому не установлено соответствие'
                        },
                        {
                            pattern: /<script [^>]*src=["'](?!\{\{|(https?:)?\/\/)/,
                            message: 'В <script> обнаружен статический ресурс, которому не установено соответсвие'
                        },
                        {
                            pattern: /<img [^>]*src=["'](?!\{\{|(https?:)?\/\/)/,
                            message: 'В <img> обнаружен статический ресурс, которому не установлено соответствие'
                        }

                    ]

                },
                files: {
                    src: ['views/**/*.handlebars']
                }
            },
            css_statics: {
                options: {
                    rules: [
                        {
                            pattern: /url\(/,
                            message: 'В свойстве LESS обнаружен статический ресурс которому не установлено соответствие'
                        }
                    ]
                },
                files: {
                    src: [
                        'less/**/*.less'
                    ]
                }
            }
        }
    });

    //регестрация заданий
    grunt.registerTask('default', ['cafemocha', 'jshint', 'exec', 'lint_pattern']);
    grunt.registerTask('static', ['less', 'cssmin', 'uglify', 'hashres']);
}