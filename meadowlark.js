/**
 * Created by Admin on 18.01.2017.
 */

var express = require('express');
var app = express();
var fortune = require('./lib/fortune');
//установка механизма представления handlebars
var handlebars = require('express-handlebars').create({
    defaultLayout: 'main',
    helpers: {
        section: function (name, option) {
            if (!this._section) this._section = {};
            this._section[name] = option.fn(this);
            return null;
        },
        static: function (name) {
            return require('./lib/static').map(name);
        }
    }
});
var formidable = require('formidable');
var jqupload = require('jquery-file-upload-middleware');
var fs = require('fs');
var credentials = require('./credentials.js');
var cartValidation = require('./lib/cartValidation.js');
var Vacation = require('./model/vacation');
var VacationInSeasonListener = require('./model/vacationInSeasonListener');
var vhost = require('vhost');
var Dealer = require('./models/dealer');

var twitter = require('./lib/twitter')({
    consumerKey: credentials.twitter.consumerKey,
    consumerSecret: credentials.twitter.consumerSecret
});

var mongoose = require('mongoose');
var options = {
    server: {
        socketOptions: { keepAlive: 1 }
    }
};
switch(app.get('env')){
    case 'development':
        mongoose.connect(credentials.mongo.development.connectionString, options);
        break;
    case 'production':
        mongoose.connect(credentials.mongo.production.connectionString, options);
        break;
    default:
        throw new Error('Unknown execution environment: ' + app.get('env'));
};

require('./routes.js')(app);

var admin = express.Router();
app.use(require('vhost')('admin.*', admin))
admin.get('/', function (req, res) {
    res.render('admin/home');
});
admin.get('/users', function (req, res) {
    res.render('admin/users');
});

//var MongoSessionStore = require('session-mongoose')(require('connect'));
//var sessionStore = new MongoSessionStore({url: credentials.mongo[app.get('env')].connectionString});

// app.use(require('cookie-parser')(credentials.cookieSecret));
// app.use(require('express-session')({
//     resave: false,
//     saveUninitialized: false,
//     secret: credentials.cookieSecret,
//     store: SessionStore
// }));

app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');

app.set('port', process.env.PORT || 3000);

app.use(express.static(__dirname + '/public'));

app.use(function (req, res, next) {
    res.locals.showTests = app.get('env') !== 'production' &&
            req.query.test == '1';
    next();
});

app.use(require('body-parser').urlencoded({extended: true}));

app.use(require('cookie-parser')(credentials.cookieSecret));
app.use(require('express-session')({
    resave: false,
    saveUninitialized: false,
    secret: credentials.cookieSecret
}));

app.use(require('csurf')());
app.use(function (req, res, next) {
    res.locals._csrfToken = req.csrfToken();
    next();
});

app.use(cartValidation.checkWaivers);
app.use(cartValidation.checkGuestCounts);

app.use(require('cors')());
app.use('/api', require('cors')());

var tours = [
    {id: 0, name: 'Река Мана', price: 3900},
    {id: 1, name: 'Санаторий Шушенское', price: 3500}
];

// initialize dealers
Dealer.find({}, function(err, dealers){
    if(dealers.length) return;

    new Dealer({
        name: 'Oregon Novelties',
        address1: '912 NW Davis St',
        city: 'Portland',
        state: 'OR',
        zip: '97209',
        country: 'US',
        phone: '503-555-1212',
        active: true,
    }).save();

    new Dealer({
        name: 'Bruce\'s Bric-a-Brac',
        address1: '159 Beeswax Ln',
        city: 'Manzanita',
        state: 'OR',
        zip: '97209',
        country: 'US',
        phone: '503-555-1212',
        active: true,
    }).save();

    new Dealer({
        name: 'Aunt Beru\'s Oregon Souveniers',
        address1: '544 NE Emerson Ave',
        city: 'Bend',
        state: 'OR',
        zip: '97701',
        country: 'US',
        phone: '503-555-1212',
        active: true,
    }).save();

    new Dealer({
        name: 'Oregon Goodies',
        address1: '1353 NW Beca Ave',
        city: 'Corvallis',
        state: 'OR',
        zip: '97330',
        country: 'US',
        phone: '503-555-1212',
        active: true,
    }).save();

    new Dealer({
        name: 'Oregon Grab-n-Fly',
        address1: '7000 NE Airport Way',
        city: 'Portland',
        state: 'OR',
        zip: '97219',
        country: 'US',
        phone: '503-555-1212',
        active: true,
    }).save();
});

function geocodeDealer(dealer) {
    var addr = dealer.getAddress(' ');
    if (addr === dealer.geocodedAddress) return;

    if (dealerCache.geocodeCount >= dealerCache.geocodeLimit) {
        if (Date.now() > dealerCache.geocodeCount + 24 * 60 * 60 * 1000) {
            dealerCache.geocodeBegin = Date.now();
            dealerCache.geocodeCount = 0;
        } else {
            return;
        }
    }

    var geocode = require('./lib/geocode');
    geocode(addr, function (err, coords) {
        if (err) return console.log('Геокоирование потерпело неудачу ' + addr);
        dealer.lat = coords.lat;
        dealer.lng = coords.lng;
        dealer.save();
    });
}

function dealersToGoogleMaps(dealers) {
    var js = 'function addMarkers(map){\n' +
            'var markers = [];\n' +
            'var Marker = google.maps.Marker;\n' +
            'var LatLng = google.maps.LatLng;\n';
    dealers.forEach(function (d) {
        var name = d.name.replace(/'/, '\\\'').replace(/\\/,'\\\\');
        js += 'markers.push(new Marker({\n' +
                '\tposition: new LatLng(' +
                d.lat + ', ' + d.lng + '),\n' +
                '\tmap: map,\n' +
                '\title:\'' + name.replace(/'/, '\\') + '\',\n' +
                '}));\n';
    });
    js += '}';
    return js;
}

//Дилер кэш
var dealerCache = {
    lastRefreshed: 0,
    refreshInterval: 60*60*1000,
    jsonUrl: '/dealer.json',
    geocodeLimit: 2000,
    geocodeCount: 0,
    geocodeBegin: 0
};
dealerCache.jsonFile = __dirname + '/public' + dealerCache.jsonUrl;
dealerCache.refresh = function (cb) {
    if (Date.now() > dealerCache.lastRefreshed + dealerCache.refreshInterval){
        //обновление кэша
        Dealer.find({active: true}, function (err, dealers) {
            if (err) return console.log('Ошибка выборки дилеров: ' + err);
            dealers.forEach(geocodeDealer);
            // теперь мы пишем все дилеры в наш кэшированный файл json
            fs.writeFileSync(dealerCache.jsonFile, JSON.stringify(dealers));
            fs.writeFileSync(__dirname + '/public/js/dealers-googleMapMarkers.js', dealersToGoogleMaps(dealers));
            cb();
        });
    }
};
function refreshDealerCacheForever() {
    dealerCache.refresh(function () {
        setTimeout(refreshDealerCacheForever, dealerCache.refreshInterval);
    });
}
// создать пустой кэш, если он не существует, чтобы предотвратить ошибку 404
if (!fs.existsSync(dealerCache.jsonFile)) fs.writeFileSync(JSON.stringify([]));
//запуск обновления кэша
refreshDealerCacheForever();

function getWeatherData() {
    return {
        locations: [
            {
                name: 'Красноярск',
                forecastUrl: 'https://www.wunderground.com/q/zmw:00000.115.35138',
                iconUrl: 'http://icons-ak.wxug.com/i/c/k/cloudy.gif',
                weather: 'Ясно',
                temp: '-25C'
            }
        ]
    };
}

app.use(function (req, res, next) {
    if (!res.locals.partials) res.locals.partials ={};
    res.locals.partials.weatherContext = getWeatherData();
    next();
});

app.use(function (req, res, next) {
    res.locals.flash = req.session.flash;
    delete req.session.flash;
    next();
});

app.use('/email', function () {

})

app.use('/upload', function (req, res, next) {
    var now = Date.now();
    jqupload.fileHandler({
        uploadDir: function () {
            return __dirname + '/public.uploads/' + now;
        },
        uploadUrl: function () {
            return '/uploads/' + now;
        }
    })(req, res, next);
});

var Attraction = require('./model/attraction');
var Rest = require('connect-rest');

//api configuration
var apiOption = {
    context: '/',
    domain: require('domain').create()
};

apiOption.domain.on('error', function (err) {
    console.log('API domain error.\n', err.stack);
    setTimeout(function () {
        console.log('Остановка сервера после ошибки домена API');
        process.exit(1);
    }, 5000);
    server.close();
    var worker = require('cluster').worker;
    if (worker) worker.disconnect();
});

var rest = Rest.create(apiOption);

//аутентефикация
var auth = require('./lib/auth')(app, {
    baseUrl: process.env.BASE_URL,
    providers: credentials.authProviders,
    successRedirect: '/account',
    failureRedirect: '/unauthorized'
});
auth.init();
auth.registerRoutes();

function customerOnly(req, res, next) {
    if (req.user && req.user.role === 'customer') return next();
    res.redirect(303, '/unauthorized');
}
function employeeOnly(req, res, next) {
    if(req.user && req.user.role === 'employee') return next();
    next('route');
}
function allow(roles) {
    return function (req, res, next) {
        if (req.user && roles.split(',').indexOf(req.user.role) !== -1) return next();
        res.redirect(303, '/unauthorized');
    };
}

app.get('/unauthorized', function (req, res) {
    res.status(403).render('unauthorized');
});

//маршруты клиентов
app.get('/account', allow('customer,employee'), function (req, res) {
    res.render('account', {username: req.user.name});
});
app.get('/account/order-history', customerOnly, function (req, res) {
    res.render('account/order-history');
});
app.get('/account/email-prefs', customerOnly, function (req, res) {
    res.render('account/email-prefs');
});

//маршруты сотрудников
app.get('/sales', employeeOnly, function (req, res) {
    res.render('sales');
});

//интеграция с твитером
var topTweets = {
    count: 10,
    lastRefreshed: 0,
    refreshInterval: 15*60*1000,
    tweets: []
};
function getTopTweets(cb) {
    if (Date.now() < topTweets.lastRefreshed + topTweets.refreshInterval) {
        return setImmediate(function () {
            cb(topTweets.tweets);
        });
    }
    twitter.search('#travel', topTweets.count, function (result) {
        var formattedTweets = [];
        var embedOpts = {omit_script: 1};
        var promises = result.statuses.map(function (status) {
            return Q.Promise(function (resolve) {
                twitter.embed(status.id_str, embedOpts, function (embed) {
                    formattedTweets.push(embed.html);
                    resolve();
                });
            });
        });
        Q.all(promises).then(function () {
            topTweets.lastRefreshed = Date.now();
            cb(topTweets.tweets  =formattedTweets);
        });
    });
}
app.use(function (req, res, next) {
    getTopTweets(function (tweets) {
        res.locals.topTweets = tweets;
        next();
    });
});


rest.get('/attractions', function (req, content, cb) {
    Attraction.find({approved: true}, function (err, attraction) {
        if (err) return cb({error: 'Внутренняя ошибка'});
        cb(null, attraction.map(function (a) {
            return {
                name: a.name,
                description: a.description,
                location: a.location
            };
        }));
    });
});

rest.post('/attraction', function (req, content, cb) {
    var a = new Attraction({
        name: req.body.name,
        description: req.body.description,
        location: {lat: req.body.lat, lng: req.body.lng},
        history: {
            event: 'created',
            email: req.body.email,
            date: new Date()
        },
        approved: false
    });
    a.save(function (err, a) {
        if (err) return cb({error: 'Не возможно добавить достопремечательность'});
        cb(null, {id: a._id});
    });
});

rest.get('/attraction/:id', function (req, content, cb) {
    Attraction.findById(req.params.id, function (err, a) {
        if (err) return cb({error: 'Невозможно извлечь достопремечательность'});
        cb(null, {
            name: a.name,
            description: a.description,
            location: a.location
        });
    });
});

app.use(vhost('api.*', rest.processRequest()));
//app.use(require('vhost')('api.*', rest));

var autoViews={};
var fs = require('fs');
app.use(function (req, res, next) {
    var path = req.path.toLowerCase();
    if (autoViews[path]) return res.render(autoViews[path]);
    if (fs.existsSync(__dirname + '/views' + path + '.handlebars')) {
        autoViews[path] = path.replace(/^\//,'');
        return res.render(autoViews[path]);
    }
    next();
});


//user page 404
app.use(function (req, res) {
    res.status(404);
    res.render('404');
});

//user page 500
app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500);
    res.render('500');
});
app.listen(app.get('port'), function () {
    console.log('Express запушен на http:' + app.get('port'));
});
