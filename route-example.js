var app = require('express')();

app.use(function (req, res, next) {
    console.log('\n\nВсегда');
    next();
});

app.get('/a', function (req, res) {
    console.log('/a: маршрут завершен');
    res.send('a');
});

app.get('/a', function (req, res) {
    console.log('/a: никогда не вызывается');
});

app.get('/b', function (req, res, next) {
   console.log('/b: маршрут не завершен');
    next();
});

app.use(function (req, res, next) {
    console.log('Иногда');
});

