/**
 * Created by Admin on 19.01.2017.
 */

suite('Global Tests', function () {
    test('У данной страницы допустимый заголовок', function () {
        assert(document.title && document.title.match(/\S/) &&
            document.title.toUpperCase() !== 'TODO');
    });
});