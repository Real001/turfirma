var assert = require('chai').assert;
var http = require('http');
var rest = require('restler');

suite('API tests', function () {
    var attraction = {
        lat: 45.516011,
        lng: -122.682062,
        name: 'Государственный Эрмитаж',
        description: 'Не упустите возможность посмотреть один из крупнейших ' +
        'и самых значительных художественных и культурно-исторических музеев России и мира.',
        email: 'test@seaofdreamtravel.ru'
    };

    var base = 'http://localhost:3000';

    test('проверка возможности добавления достопремичательности', function (done) {
            rest.post(base + '/api/attraction', {data: attraction}).on('success', function (data) {
                assert.match(data.id, /\w/, 'должен быть задан id');
                done();
            });
    });

    test('проверка возможности извлечения достопремечательности', function (done) {
        rest.post(base + '/api/attraction', {data: attraction}).on('success', function (data) {
            rest.get(base + '/api/attraction/' + data.id).on('success', function (data) {
                assert(data.name === attraction.name);
                assert(data.description === attraction.description);
                done();
            })
        })
    });
});