/**
 * Created by Admin on 19.01.2017.
 */

var Browser = require('zombie').assert = require(chai).assert;
var browser;

suite('Межстраничные тесты', function () {
    setup(function () {
        browser = new Browser();
    });

    test('запрос расценок для груп со страницы туров по реке Мана' +
        'должен заполнятся после реферера', function (done) {
        var referrer = 'http://localhost:3000/tours/mana-river';
        browser.visit(referrer, function () {
            browser.clickLink('.requestGroupRate', function () {
                assert(browser.field('referrer').value === referrer);
                done();
            });
        });
    });

    test('запрос расценок для груп со страницы туров санатория "Шушенское"' +
        'должен заполнятся после реферера', function (done) {
        var referrer = 'http://localhost:3000/tours/shushenskoe';
        browser.visit(referrer, function () {
            browser.clickLink('.requestGroupRate', function () {
                assert(browser.field('referrer').value === referrer);
                done();
            });
        });
    });

    test('посещение страницы "Запрос цены для групп" напрямую' +
        'должен приводить к пустому полю реферера', function (done) {
        browser.visit('http://localhost:3000/tours/request-group-rate',
            function () {
                assert(browser.field('referrer').value === '');
            });
    });

});