var fortune = require('../lib/fortune');
var expect = require('chai').expect;

suite('Тест печений-предсказаний', function () {
    test('getFortune() должна возвращать предсказание', function () {
        expect(typeof fortune.getFortune() === 'string');
    });
});